from distutils.core import setup
import py2exe, sys

sys.argv.append('py2exe')

class Target:
    def __init__(self, **kw):
        self.__dict__.update(kw)
        self.version = "1.0.0"
        self.company_name = "Adlens"
        self.copyright = "Copyright (c)2016 Will Guest"
        self.product_name = "Station 10 Assistant"

target = Target(
    description = "Focuss Quality Product Release Software",
    script = "Assistant10.py",
    dest_base = "Assistant10")

setup(
    options = {'py2exe':{'bundle_files': 2, 'compressed': False,
                'includes':['sip', 'decimal'],
                'excludes':['Tkconstants', 'tkinter', 'tcl']}},
    zipfile=None,
    windows = [target]
)