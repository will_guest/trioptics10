#-------------------------------------------------------------------------------
# Name:        Module 10 Packaged Functions
# Purpose:     Taking optical measurements of Focuss product as part of
#               product release testing.
# Author:      William Guest
# Created:     26/03/2015
# Copyright:   (c) Will Guest 2015
#-------------------------------------------------------------------------------
#!/usr/bin/env python
#.-*- coding: utf-8 -*-

import time, sys
from PyQt5 import uic, Qt, QtCore, QtGui

from Imports.TriopticsCommands import TOP
from Imports.JobInfo import *
from certConf import *
from Report10 import RepWindow

from win32api import keybd_event
from win32con import KEYEVENTF_KEYUP
from os import path, getcwd

app = None
guiFile = path.join(path.dirname(sys.argv[0]), 'Assistant10.ui')
form_class = uic.loadUiType(guiFile, True)[0]

class AppWindow(QtGui.QMainWindow, form_class):

    def __init__(self, parent=None):
        # Initialise main window
        QtGui.QMainWindow.__init__(self, parent=None)
        QtGui.QMainWindow.setWindowFlags(self, QtCore.Qt.WindowStaysOnTopHint
                                                    |QtCore.Qt.WindowTitleHint)
        QtGui.QMainWindow.move(self, 1330, 20)
        self.setupUi(self)
        QtCore.pyqtRemoveInputHook()

        # Binding event handlers (for buttons)
        self.btnBarcode.clicked.connect(self.input_barcode)
        self.btnClrRes.clicked.connect(lambda: self.tblTOP.clearContents())
        self.btnClrResFoc.clicked.connect(self.clear_table_FOC)

        self.btnRU.clicked.connect(lambda: self.btnTOP_measure('RU', 0))
        self.btnRA.clicked.connect(lambda: self.btnTOP_measure('RA', 1))
        self.btnLU.clicked.connect(lambda: self.btnTOP_measure('LU', 2))
        self.btnLA.clicked.connect(lambda: self.btnTOP_measure('LA', 3))
        self.btnReport.clicked.connect(self.btnReport_clicked)

        self.R_Mod_ID.textChanged.connect(self.get_mod_no)

        # Enable Hover Options for Ref. Lens Labels
        self.lblRU_Ref.installEventFilter(self)
        self.lblRA_Ref.installEventFilter(self)
        self.lblLU_Ref.installEventFilter(self)
        self.lblLA_Ref.installEventFilter(self)

    def eventFilter(self, object, event):
        if (event.type() == QtCore.QEvent.Enter):
            eval("self.grp" + object.objectName()[3:5] + ".raise_()")
        if (event.type() == QtCore.QEvent.Leave):
            eval("self.grp" + object.objectName()[3:5] + ".lower()")
        return QtGui.QWidget.eventFilter(self, object, event)

    def enable_buttons(self):
        self.grpMeasBut.setEnabled(True)
        self.btnBarcode.setEnabled(True)
        self.btnClrRes.setEnabled(True)
        self.btnClrResFoc.setEnabled(True)
        self.btnReport.setEnabled(True)
        QtGui.QApplication.processEvents()

    def disable_buttons(self):
        self.grpMeasBut.setDisabled(True)
        self.btnBarcode.setDisabled(True)
        self.btnClrRes.setDisabled(True)
        self.btnClrResFoc.setDisabled(True)
        self.btnReport.setDisabled(True)
        QtGui.QApplication.processEvents()

    def clear_table_FOC(self):
        for row in range(self.tblFOC.rowCount()):
            for col in range(self.tblFOC.columnCount()):
                toRead = QtGui.QTableWidgetItem(self.tblFOC.item(row, col))
                if toRead.backgroundColor().value() == 0:
                    self.tblFOC.item(row, col).setText("")


    #----------------------- INPUT FUNCTIONS ---------------------------------#

    def prompt_user1(self, msg = None):
        msgBox = QtGui.QMessageBox()
        msgBox.setWindowTitle("Attention")
        msgBox.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        msgBox.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        msgBox.setText(msg)
        msgBox.exec_()

    def prompt_user2(self, msg = None):
        msgBox = QtGui.QMessageBox()
        msgBox.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        msgBox.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        msgBox.setText(msg)
        msgBox.addButton(QtGui.QPushButton('OK'), QtGui.QMessageBox.YesRole)
        msgBox.addButton(QtGui.QPushButton('Cancel'), QtGui.QMessageBox.NoRole)
        return msgBox.exec_()

    def input_barcode(self):
        global grabJob
        grabJob = QtGui.QDialog(self)
        grabJob.setFixedSize(250,25)
        grabJob.setWindowTitle("Enter Job Number")
        grabJob.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        grabJob.jobInput = QtGui.QLineEdit(grabJob)
        grabJob.jobInput.resize(250,25)
        grabJob.show()
        self.btnBarcode.setDisabled(True)

        grabJob.jobInput.textChanged.connect(self.get_job_data)
        grabJob.jobInput.destroyed.connect(self.enable_buttons)

    def get_mod_no(self):
        if self.R_Mod_ID.cursorPosition() == 9:
           self.L_Mod_ID.setFocus()
           self.L_Mod_ID.selectAll()

    def get_job_data(self, jobID):
        if grabJob.jobInput.cursorPosition() == 6:
            jobID = str(grabJob.jobInput.text())
            grabJob.close()
            jobData = []

            try:
                int(jobID)
                jobData = JobInfo.get_data(jobID)
                if jobData == "Invalid Job Number":
                    self.lineJobID.setText("")
                else:
                    self.populate_form(jobData)
                    self.R_Mod_ID.setFocus()
                    self.R_Mod_ID.selectAll()
            except ConnectionError:
                self.prompt_user1("Error when trying to connect to database...")
            except ValueError:
                self.prompt_user1("Error entering job number, please try again")
            except IOError:
                self.prompt_user1("Job number not found in Annapurna")
            except TypeError:
                self.prompt_user1("Job number not found in Annapurna")

    def populate_form(self, jobData):

        jobID = QtGui.QTableWidgetItem(str(jobData[0]))
        storeID = QtGui.QTableWidgetItem(str(jobData[1]))
        frmStl = QtGui.QTableWidgetItem(str(jobData[2]))
        frmClr = QtGui.QTableWidgetItem(str(jobData[3]))

        RtPD = QtGui.QTableWidgetItem(str(jobData[4]))
        LtPD = QtGui.QTableWidgetItem(str(jobData [5]))
        RtSph = QtGui.QTableWidgetItem(str(jobData[6]))
        LtSph = QtGui.QTableWidgetItem(str(jobData[7]))
        RtCyl = QtGui.QTableWidgetItem(str(jobData[8]))
        LtCyl = QtGui.QTableWidgetItem(str(jobData[9]))
        RtAxs = QtGui.QTableWidgetItem(str(jobData[10]))
        LtAxs = QtGui.QTableWidgetItem(str(jobData[11]))
        RtAdd = QtGui.QTableWidgetItem(str(jobData[12]))
        LtAdd = QtGui.QTableWidgetItem(str(jobData[13]))

        self.tblJob.setItem(0, 0, jobID)
        self.tblJob.setItem(0, 1, frmStl)
        self.tblJob.setItem(0, 2, frmClr)

        self.tblRx.setItem(0,0, RtPD)
        self.tblRx.setItem(1,0, LtPD)
        self.tblRx.setItem(0,1, RtSph)
        self.tblRx.setItem(1,1, LtSph)
        self.tblRx.setItem(0,2, RtCyl)
        self.tblRx.setItem(1,2, LtCyl)
        self.tblRx.setItem(0,3, RtAxs)
        self.tblRx.setItem(1,3, LtAxs)
        self.tblRx.setItem(0,4, RtAdd)
        self.tblRx.setItem(1,4, LtAdd)

        self.handle_rx_data()

    def choose_fixture_ID(self, frameStyle):
        if   frameStyle == "Kinetic":   return 'ADE 0727'
        elif frameStyle == "Quantum":   return 'ADE 0728'
        elif frameStyle == "Flux":      return 'ADE 0729'
        elif frameStyle == "Vectra":    return 'ADE 0730'
        elif frameStyle == "Torsion":   return 'ADE 0762'
        else:                           return "Not found"



    #-------------------- SET CONFIG & IDEAL POSITIONS -----------------------#

    def set_range(self, state, pow1, pow2, Sep):

        if pow1 <= -1.75:   RefL = 0; obj1 = obj2 = 'PL800'
        elif pow2 >= 2.5:   RefL = 0; obj1 = obj2 = 'AMT300'
        elif pow2 >= 0.5:   RefL = 2; obj1 = obj2 = 'AMT300'
        else:               RefL = 5; obj1 = obj2 = 'AMT300'
        eval("self.lbl" + state + "_Ref.setText('" + str(RefL) + "D')")

        focal1 = round(1000/-(pow1 + RefL - (Sep*pow1*RefL)), 3)
        focal2 = round(1000/-(pow2 + RefL - (Sep*pow2*RefL)), 3)

        eval("self.lblRange" + state + "_ver.setText(str(" + str(focal1) + "))")
        if focal1 < -500:   obj1 = 'PL-200'
        eval("self.lbl" + state + "V_Obj.setText('" + obj1 + "')")

        eval("self.lblRange" + state + "_hor.setText(str(" + str(focal2) + "))")
        if focal2 < -500:   obj2 = 'PL-200'
        eval("self.lbl" + state + "H_Obj.setText('" + obj2 + "')")

    def handle_rx_data(self):
        rxData = []
        for row in range(self.tblRx.rowCount()):
            for col in range(self.tblRx.columnCount()):
                try:
                    if (self.tblRx.item(row, col).text() == 'None' or
                        self.tblRx.item(row, col).text() == ''):
                        self.tblRx.setItem(row, col, QtGui.QTableWidgetItem('0.0'))
                    rxData.append(float(self.tblRx.item(row, col).text()))
                except:
                    self.prompt_user1("Prescription data incomplete")
                    return

        (rSph, rCyl, rAdd) = (rxData[1], rxData[2], rxData[4])
        (lSph, lCyl, lAdd) = (rxData[6], rxData[7], rxData[9])

        # Calc Ideal Positions, Update GUI
        self.set_range('RU', rSph, rSph + rCyl, 0.011)
        self.set_range('RA', rSph + rAdd, rSph + rCyl + rAdd, 0.011)
        self.set_range('LU', lSph, lSph + lCyl, 0.011)
        self.set_range('LA', lSph + lAdd, lSph + lCyl + lAdd, 0.011)

        QtGui.QApplication.processEvents()



    #----------------------- MEASUREMENT SUPPORT -----------------------------#

    def choose_offset(self, obj):
        if obj == 'AMT300':     return -246.54
        elif obj == 'PL-200':   return 255.16
        elif obj == "PL800":    return -745.14

    def init_design_file(self, state):
        resultconn = TOP.tcp_conn('10.0.20.24', 23882)

        objV = eval("self.lbl" + state + "V_Obj.text()")
        objH = eval("self.lbl" + state + "H_Obj.text()")
        designFile = ("C:\Program Files\TRIOPTICS GmbH\Design Files\M10." +
                        str(objV) + "." + str(objH) + ".df")
        TOP.open_df(designFile)

        posVer = round(float(eval("self.lblRange" + state + "_ver.text()")),3)
        posHor = round(float(eval("self.lblRange" + state + "_hor.text()")),3)
        TOP.set_ml_radius(1, posVer)
        TOP.set_ml_radius(2, posHor)

        z_corr = self.choose_offset(objV)
        pos10x = posVer + z_corr
        TOP.set_ml_radius(3, round(pos10x,3))

        TOP.calc_rel_pos()
        time.sleep(0.5)


    #------------------------- ROTATIONAL AUTOFOCUS --------------------------#

    def press(self, VAL):
        # Programmatically press 'esc'
        global VK_CODE
        VK_CODE = {'esc': 0x1B}
        keybd_event(VK_CODE[VAL], 0, 0, 0)
        time.sleep(0.05)
        keybd_event(VK_CODE[VAL], 0, KEYEVENTF_KEYUP, 0)

    def check_angle(self):
        # Rotational Autofocus thru 200 degrees (air bearing speed req'd: 10%)
        initPos = TOP.get_pos(3)
        initNum = float(initPos[:8].replace(',','.'))
        TOP.set_manual_rotation("off")
        time.sleep(1)

        # Rotate one way... then back (nearly to start)
        TOP.find_center()
        time.sleep(2)
        self.press('esc')
        time.sleep(1.2)
        TOP.find_center()
        time.sleep(1.5)
        self.press('esc')

        # Start rotation for finding axis
        TOP.set_manual_rotation("on")
        time.sleep(2.8)
        TOP.measure_surface(3)
        time.sleep(4.7)
        TOP.set_shutter(100)        # artificial dark start
        time.sleep(1)               # \/
        TOP.set_shutter(2000)       # .
        time.sleep(9.85)            # time for data-point collection
        TOP.set_shutter(100)        #  .
        time.sleep(2)               # /\
        TOP.set_shutter(2000)       #| |
        time.sleep(1)               # artificial dark stop

        recvPos = float(TOP.get_rel_pos()[:8].replace(',','.'))
        time.sleep(0.5)
        ValAxis = int(((recvPos - initNum + 0.06)*2000)-40)

        if ValAxis <= 0:    ValAxis = ValAxis + 180
        TOP.set_manual_rotation('off')
        time.sleep(0.5)
        return ValAxis

    def set_angle(self, AxisVal):
        rotTime = (AxisVal * 0.0265) + 0.249
        if rotTime < 5.02:
            TOP.find_center()
            time.sleep(rotTime)
            self.press('esc')
        else:
            self.prompt_user1("Angle: " + str(ValAxis))
        time.sleep(1)
        QtGui.QApplication.processEvents()

    def input_axis(self, state, activeRow):
        # Custom dialog for entering axis value
        inpbox = QtGui.QInputDialog()
        inpbox.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        inpbox.setWindowTitle("User Input")
        inpbox.setLabelText("Enter Axis Value (1-180)")
        inpbox.setInputMode(1)
        inpbox.setIntRange(1,180)
        inpbox.exec_()

        self.tblTOP.setItem(activeRow, 2,
            QtGui.QTableWidgetItem(str(inpbox.intValue())))
        QtGui.QApplication.processEvents()

    def is_Static(self):
        # Check whether the measurement head is moving
        firstPos = TOP.get_rel_pos()
        time.sleep(1)
        secondPos = TOP.get_rel_pos()
        while firstPos != secondPos:
            firstPos = secondPos
            time.sleep(1)
            secondPos = TOP.get_rel_pos()
        return float(secondPos[:8].replace(',','.'))

    def config_PD(self, state):
        print ("state:" + state)
        rPD = float(self.tblRx.item(0,0).text())
        lPD = float(self.tblRx.item(1,0).text())
        refL = str(eval("self.lbl" + state + "_Ref.text()"))

        print (rPD, lPD)

        if  self.tblJob.item(0,1).text() == ("Vectra" or "Torsion"):
                offsetPD = 1
        else:   offsetPD = 0

        print (offsetPD)

        if state[:1] == "R":
            airBearMic = str(4.75 + (32 - offsetPD - rPD))
            fixtureMic = str(5 - (32 - offsetPD - rPD))
        elif state[:1] == "L":
            airBearMic = str(4.75 - (32 - offsetPD - lPD))
            fixtureMic = str(5 + (32 - offsetPD - lPD))

        self.prompt_user1("Please fit " + refL + " reference lens," + '\n' +
                    "set the air-bearing micrometer to " + airBearMic  + '\n' +
                    "and set the fixture micrometer to " + fixtureMic)


    #---------------------  MEASUREMENT  ------------------------------------#

    def take_measurement(self, state, activeRow):
        #Set Initial State
        self.disable_buttons()
        self.config_PD(state)
        TOP.set_objective(eval("self.lbl" + state + "V_Obj.text()"))

        v_pos = str(float(TOP.get_pos(1)[:8].replace(',','.')))
        time.sleep(1)
        TOP.go_to(v_pos)

        cont = self.prompt_user2("Before continuing, please check" +'\n' +'\n' +
                        "1) 'Vertical' Option in Autofocus is selected" + '\n' +
                        "2) the air bearing is at its reference point." + '\n' +
                        "3) and its velocity is set to '10%'.")

        if cont == 1:   return
        foundPos = self.is_Static()

        # Rotational Autofocus
        AxisVal = self.check_angle()
        self.tblTOP.setItem(activeRow, 2, QtGui.QTableWidgetItem(str(AxisVal)))
        QtGui.QApplication.processEvents()
        time.sleep(1)

        self.set_angle(AxisVal)
        time.sleep(1)
        TOP.set_manual_rotation('on')

        # Vertical Meridian Measurement
        TOP.measure_surface(1)
        time.sleep(5)
        foundPos = self.is_Static()

        cont = self.prompt_user2("Adjust to give best vertical focus then click 'OK'")
        if cont == 1:   return
        time.sleep(1)

        newVer = float(TOP.get_rel_pos()[:8].replace(',','.'))
        TOP.set_ml_radius(1, round(newVer,3))
        TOP.calc_rel_pos()
        time.sleep(0.5)

        TOP.measure_surface(1)
        time.sleep(1)
        axisInt = self.input_axis(state, activeRow)

        time.sleep(3)
        v_pos = self.is_Static()
        TOP.set_ml_radius(1, v_pos)

        self.tblTOP.setItem(activeRow, 0, QtGui.QTableWidgetItem(str(v_pos)))
        QtGui.QApplication.processEvents()

        # Horizontal Meridian Measurement
        cont = self.prompt_user2("Choose 'Horizontal' Autofocus, then click 'OK'")
        if cont == 1:   return

        TOP.set_objective(eval("self.lbl" + state + "H_Obj.text()"))
        time.sleep(3)
        TOP.measure_surface(2)
        time.sleep(5)

        h_pos = self.is_Static()
        TOP.set_ml_radius(2, h_pos)
        self.tblTOP.setItem(activeRow, 1, QtGui.QTableWidgetItem(str(h_pos)))
        QtGui.QApplication.processEvents()

        # Ensure Correct Axis Orientation
        TOP.set_manual_rotation('off')
        time.sleep(2)
        axisInt = int(self.tblTOP.item(activeRow, 2).text())

        if v_pos-h_pos <= 0:
            if axisInt <= 90:   axisInt = axisInt + 90
            else:               axisInt = axisInt - 90
            self.tblTOP.setItem(activeRow, 2, QtGui.QTableWidgetItem(str(axisInt)))

        # 'Find Centre' and Update UI
        midFoc = (v_pos+h_pos)/2
        QtGui.QApplication.processEvents()
        time.sleep(2)
        TOP.go_to(round(midFoc,3))
        time.sleep(2)
        foundPos = self.is_Static()
        TOP.find_center()
        time.sleep(20)

        decentreX, decentreY = TOP.live_values_cam()
        self.tblTOP.setItem(activeRow, 3, QtGui.QTableWidgetItem(str(decentreX)))
        self.tblTOP.setItem(activeRow, 4, QtGui.QTableWidgetItem(str(decentreY)))
        QtGui.QApplication.processEvents()

        TOP.tcp_close()
        self.enable_buttons()

        self.prompt_user1("Measurement complete")
        QtGui.QApplication.processEvents()

    #---------------------- MEASUREMENT BUTTONS -----------------------------#

    def btnTOP_measure(self, hand, row):
        self.init_design_file(hand)
        self.take_measurement(hand, row)
        self.enable_buttons()

    #---------------------- REPORT ------------------------------------------#

    def btnReport_clicked(self):

        #Initialise Report Window
        self.rWin = RepWindow()
        self.rWin.__init__()

        # Get Info needed for test report
        jobID = self.tblJob.item(0,0).text()
        instrID_TOP = "ADE 0223"
        instrID_FOC = "ADE 0336"
        frameStyle = self.tblJob.item(0,1).text()
        frameColour = self.tblJob.item(0,2).text()
        fixtureID_TOP = self.choose_fixture_ID(frameStyle)

        # Complete Batch Information on Test Report
        self.rWin.batchInfo(jobID, frameStyle, frameColour,
                            instrID_TOP, instrID_FOC, fixtureID_TOP)

        # Write Module Numbers
        if(self.R_Mod_ID.text() and self.L_Mod_ID.text()) != "":
            self.rWin.modNos(self.R_Mod_ID.text(), self.L_Mod_ID.text())
        else:
            self.prompt_user1("FFE numbers must not be blank")
            return

        # Write Prescription Data
        rxData = []
        for row in range(self.tblRx.rowCount()):
            for col in range(self.tblRx.columnCount()):
                try:
                    rxData.append(self.tblRx.item(row, col).text())
                except:
                    self.prompt_user1("Error in prescription data")
                    return

        self.rWin.rxInfo("Rt", rxData[0:5])
        self.rWin.rxInfo("Lt", rxData[5:10])

        refLensRU = int(self.lblRU_Ref.text()[:1])
        refLensRA = int(self.lblRA_Ref.text()[:1])
        refLensLU = int(self.lblLU_Ref.text()[:1])
        refLensLA = int(self.lblLA_Ref.text()[:1])

        # Display Trioptics Measurement Data on Report
        resultsTOP = []
        for row in range(self.tblTOP.rowCount()):
            for col in range(self.tblTOP.columnCount()):
                try:
                    resultsTOP.append(float(self.tblTOP.item(row, col).text()))
                except:
                    self.prompt_user1("Trioptics data must be numbers")
                    return

        self.rWin.uMeasuredData("Rt", refLensRU, resultsTOP[0:5])
        self.rWin.aMeasuredData("Rt", refLensRA, resultsTOP[5:10])
        self.rWin.uMeasuredData("Lt", refLensLU, resultsTOP[10:15])
        self.rWin.aMeasuredData("Lt", refLensLA, resultsTOP[15:20])

        # Calculate Max. Meridional Error
        self.rWin.calcMeridErr("Rt", 0.3)
        self.rWin.calcMeridErr("Lt", 0.3)

        # Calculate Add Error
        RtAddErr = self.rWin.calcAddErr("Rt", 0.50)
        LtAddErr = self.rWin.calcAddErr("Lt", 0.50)
        self.rWin.calcNESImb(RtAddErr, LtAddErr, 0.50)

        resultsFOC = []
        for row in range(self.tblFOC.rowCount()):
            for col in range(self.tblFOC.columnCount()):
                resultsFOC.append(self.tblFOC.item(row, col).text())
        for x in (19, 18, 9, 8):    del resultsFOC[x]

        try:
            for i in range(len(resultsFOC)):
                resultsFOC[i] = float(resultsFOC[i])
        except:
            self.prompt_user1("Focimeter data must be numbers")
            return

        # Display Focimeter Measurement Data on Report
        (hPrRt, vPrRt) = self.rWin.FOCmeasuredData("Rt", resultsFOC[0:8])
        (hPrLt, vPrLt) = self.rWin.FOCmeasuredData("Lt", resultsFOC[8:16])

        # Calculate Prism Imbalance
        hTolL = -2.33
        hTolU =  3.00
        vTol  =  0.33
        self.rWin.calcPrismImb(hPrRt, hPrLt, vPrRt, vPrLt, hTolL, hTolU, vTol)

        # Include Comments
        if len(self.txtComments.toPlainText()) > 30:
            self.prompt_user1("Please limit comments to 30 characters")
            return
        self.rWin.comments(self.txtComments.toPlainText())

        # Give Final Result
        self.rWin.finalResult()

        # Show Window
        self.rWin.show()

        # Produce CofC
        self.make_CofC(rxData, resultsFOC)

    #---------------------- Make C of C -------------------------------------#

    def make_CofC(self, rxData, focData):
        from datetime import datetime

        # Collect information needed for CofC
        certDate = datetime.today().strftime('%d %b %Y')
        jobID = self.tblJob.item(0,0).text()
        frameStyle = self.tblJob.item(0,1).text()
        frameColour = self.tblJob.item(0,2).text()
        (storeNo, custRef, enterDate) = JobInfo.get_orderdata(jobID)
        orderDate = datetime.strptime(enterDate, "%y%m%d%H%M").strftime('%d %b %y')

        # Manage focimeter data: set formats, rearrange into new list
        focRtPD = rxData[0]
        focLtPD = rxData[5]
        focRtAdd = (focData[5]+(focData[6]/2)) - (focData[0]+(focData[1]/2))
        focLtAdd = (focData[13]+(focData[14]/2)) - (focData[8]+(focData[9]/2))

        optData = []
        if focData[0] >= 0:
            optData.append("+" + "{0:.2f}".format(focData[0]))
        else:
            optData.append("{0:.2f}".format(focData[0]))
        optData.append("{0:.2f}".format(focData[1]))
        optData.append(str(int(focData[2])))
        optData.append("{0:.2f}".format(float(focRtAdd)))
        optData.append(focRtPD)
        if focData[8] >= 0:
            optData.append("+" + "{0:.2f}".format(focData[8]))
        else:
            optData.append("{0:.2f}".format(focData[8]))

        optData.append("{0:.2f}".format(focData[9]))
        optData.append(str(int(focData[10])))
        optData.append("{0:.2f}".format(float(focLtAdd)))
        optData.append(focLtPD)

        try:
            CreateCertificate.makeForm(jobID, rxData, optData, storeNo,
                        certDate, custRef, orderDate, frameStyle, frameColour)

        except PermissionError:
            self.prompt_user1("CofC is open, please close it and re-open report")

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    myWindow = AppWindow(None)
    myWindow.show()
    sys.exit(app.exec_())