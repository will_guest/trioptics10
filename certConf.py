from os import path 
import reportlab.rl_settings
from reportlab.platypus import *
from reportlab.lib import colors
from reportlab.lib.enums import *
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont


class RotatedPara(Paragraph):
	def draw(self):
		self.canv.saveState()
		self.canv.translate(82*mm,8*mm)
		self.canv.rotate(270)
		Paragraph.draw(self)
		self.canv.restoreState()

class CreateCertificate():

    def drawRot(self):
        self.canv.saveState()
        self.canv.translate(82*mm,8*mm)
        self.canv.rotate(270)
        Paragraph.draw(self)
        self.canv.restoreState()

    def makeForm(jobNo, rxData, opData, 
                 storeNo, certDate, prodPartNo, patOrderNo, custRef, orderDate, frameStyle, frameColour,):

        # Register Fonts
        pdfmetrics.registerFont(TTFont('Arial', 'fonts/arial.ttf'))
        pdfmetrics.registerFont(TTFont('Arial-Bold', 'fonts/arialbd.ttf'))

        #patientData = [custRef, prodPartNo, patOrderNo, storeNo, orderDate, certDate, frameStyle, certDate, frameColour]
        #outfilename = str(jobNo) + "_" + custRef + ".pdf"
        #outfiledir = 'Q:\Releasing Focuss Batches\Batch Release\Focuss New CofCs'

        # test file location
        outfilename = "devTest.pdf"
        outfiledir = 'C:\TOP10Testing'
        
        outfilepath = path.join(outfiledir, outfilename)

        doc = BaseDocTemplate(outfilepath,
                            leftMargin=20,topMargin=20,rightMargin=20,
                            bottomMargin=20,showBoundary=0)

        styles = getSampleStyleSheet()

        styles.add(ParagraphStyle(name='Right', alignment=TA_RIGHT, fontSize=8))

        styleN = styles["Normal"]
        styleR = styles["Right"]
        styleL = ParagraphStyle(name='Left', alignment=TA_LEFT, fontSize=8, leftIndent=5)
        styleC = ParagraphStyle(name='Centre', alignment=TA_CENTER, fontSize=8, leftIndent=35, rightIndent=35)
        styleSL = ParagraphStyle(name='SmallLt', alignment=TA_LEFT, fontSize=7)
        styleSR = ParagraphStyle(name='SmallRt', alignment=TA_RIGHT, fontSize=6)
        styleT = ParagraphStyle(name='pTitle', alignment=TA_LEFT, fontSize=12)

        Story=[]

        # Images
        aflogo = Image("Images//allfield-logo.png", 15*mm, 15*mm, hAlign='LEFT')
        afname = Image("Images//allfield-name.png", (198/5)*mm, (58/5)*mm, hAlign='LEFT')
        factoryIcon = Image('Images//factory-icon.png', 10*mm, 10*mm)
        CElogo = Image("Images//CERx.png", (81/3.3)*mm, (38/3.3)*mm, hAlign='RIGHT')


        # Size Frames on Page
        (lMgn, tMgn, bMgn) = (doc.leftMargin, doc.topMargin, doc.bottomMargin)
        frmTop1 = Frame(lMgn, tMgn+230*mm, doc.width/2-6, doc.height/5)
        frmTop2 = Frame(lMgn+doc.width/2+6, tMgn+230*mm, doc.width/2-6, doc.height/5)
        frmMain = Frame(lMgn, bMgn+50*mm, doc.width, doc.height-105*mm)

        frmBtm1 = Frame(lMgn, bMgn, doc.width/2+12, doc.height/6)
        frmBtm2 = Frame(lMgn+doc.width/2-12, bMgn, doc.width/2+12, doc.height/6)

        doc.addPageTemplates([PageTemplate(id='PageOne',frames=[frmTop1, frmTop2, frmMain, frmBtm1, frmBtm2])])
        Story.append(NextPageTemplate('PageOne'))

        # Put Data into Tables and Set Styles
        dataRx = [['Ordered Prescription', 'Sphere', 'Cylinder', 'Axis', 'Add'],
                    ['OD (Right)', rxData[1], rxData[2], rxData[3],  rxData[4]],
                    ['OS (Left)' , rxData[6], rxData[7], rxData[8],  rxData[9]]]

        dataOpt = [['Optical Prescription', 'Sphere', 'Cylinder', 'Axis', 'Add'],
                    ['OD (Right)', opData[0], opData[1], opData[2],  opData[3]],
                    ['OS (Left)' , opData[5], opData[6], opData[7],  opData[8]]]

        dataPat = [['Certificate Number', jobNo, 'Patient Name', custRef],
                    ['Production Part Number', prodPartNo, 'Patient Order Number', patOrderNo],
                    ['Batch Number', storeNo, 'Patient Order Date', orderDate],
                    ['Store Number', certDate, 'Frame Style', frameStyle],
                    ['Date of Certification', certDate, 'Frame Colour', frameColour]]

        dataRv1 = [['Quality Assurance Approval for Release']]
        dataRv2 = [['Title', 'Print Name', 'Signature', 'Date'], ['','','','']]

        dataManFac = [[ 'Manufactured by  ', 
                            'Allfield, \n'
                            'Chilbrook, 1 Oasis Park, \n'
                            'Stanton Harcourt Road, \n'
                            'Eynsham, Oxfordshire OX29 4TP \n'
                            'United Kingdom',
                        'Product Description', 
                        'Custom made adjustable eyewear']]

        tRx = Table(dataRx, colWidths=(30*mm,15*mm,15*mm,10*mm,10*mm),
                            rowHeights=3*[4*mm], hAlign='LEFT')
        tOpt = Table(dataOpt, colWidths=(30*mm,15*mm,15*mm,10*mm,10*mm),
                            rowHeights=3*[4*mm], hAlign='LEFT')
        tPat = Table(dataPat, colWidths=47*mm, rowHeights=5*[4*mm])

        tPaD = Table(dataManFac, colWidths=47*mm, rowHeights=18*mm)

        tRv1 = Table(dataRv1, colWidths=184*mm, rowHeights=None)
        tRv2 = Table(dataRv2, colWidths=46*mm, rowHeights=(5*mm, 8*mm))

        tstyle1 = TableStyle([('GRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('ALIGN', (1,0), (-1,-1),'CENTER'),
                            ('VALIGN', (0,0), (-1,-1),'MIDDLE'),
                            ('TEXTCOLOR', (0,0), (-1,0), colors.black),
                            ('BACKGROUND', (0,0), (-1,0), colors.lightblue),
                            ('FONTSIZE', (0,0), (-1,-1), 7)])

        tstylePatInfo = TableStyle([('GRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('VALIGN', (0,0), (-1,-1),'TOP'),
                            ('TEXTCOLOR', (0,0), (-1,-1), colors.black),
                            ('BACKGROUND', (0,0), (0,-1), colors.lightblue),
                            ('BACKGROUND', (2,0), (2,-1), colors.lightblue),
                            ('FONTSIZE', (0,0), (-1,-1), 7)])

        tstyleMan = TableStyle([('GRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('VALIGN', (0,0), (-1,-1),'MIDDLE'),
                            ('BACKGROUND', (0,0), (0,-1), colors.lightblue),
                            ('BACKGROUND', (2,0), (2,-1), colors.lightblue),
                            ('FONTSIZE', (0,0), (0,-1), 7),
                            ('FONTSIZE', (1,0), (1,-1), 6),
                            ('FONTSIZE', (2,0), (2,-1), 7),
                            ('FONTSIZE', (3,0), (3,-1), 7),
                            ('LEADING', (0,0), (-1,-1), 8)])

        tstyle3 = TableStyle([('BOX', (0,0), (-1,-1), 0.25, colors.black),
                            ('ALIGN', (0,0), (-1,-1),'CENTER'),
                            ('VALIGN', (0,0), (-1,-1),'MIDDLE'),
                            ('BACKGROUND', (0,0), (-1,-1), colors.lightblue),
                            ('FONT', (0,0), (-1,-1),'Arial-Bold'),
                            ('FONTSIZE', (0,0), (-1,-1), 8),
                            ('TEXTCOLOR', (0,0), (-1,-1), colors.black)])
        
        tRx.setStyle(tstyle1)
        tOpt.setStyle(tstyle1)
        tPat.setStyle(tstylePatInfo)
        tPaD.setStyle(tstyleMan)
        tRv1.setStyle(tstyle3)
        tRv2.setStyle(tstyle1)


         # ------------------------- Write to Page ----------------------------- #

         # Populate Double Column at Top (logo & Rx Info)
        Story.append(Spacer(1, 12))
        Story.append(aflogo)
        Story.append(Spacer(1, 90))

        Story.append(Paragraph("QS-F-004.rev7.0", styleR))
        Story.append(Spacer(1, 6))
        Story.append(Indenter(left=5*mm))
        Story.append(Paragraph("Doctor's Prescription", styleL))
        Story.append(tRx)
        Story.append(Spacer(1, 12))
        para = RotatedPara(jobNo, styleL)
        Story.append(para)
        Story.append(Paragraph("Optical Compensation**", styleL))
        Story.append(tOpt)
        Story.append(Spacer(1, 9)) # minimise this for more space below
        
         # Populate Single Column
        Story.append(Indenter(left=-2*mm))
        ptxt = '<b> Allfield</b><super>TM </super><b> Quality Assurance Certificate </b>'
        Story.append(Paragraph(ptxt, styleT))

        Story.append(Spacer(1, 12))
        Story.append(tPat)
        Story.append(tPaD)
        Story.append(Indenter(left=2*mm))
        Story.append(Spacer(1, 12))

        Story.append(Paragraph("This certificate confirms that product serial "
                        "number: %s provides the following optical compensation*:"
                        % (jobNo), styleL))

         # Modify Prescription table to include PD
        dataRx[0].append('PD')
        dataRx[1].append(rxData[0])
        dataRx[2].append(rxData[5])
        tOrd2 = Table(dataRx, colWidths=(50*mm,30*mm,30*mm,25*mm,25*mm,25*mm),
                                rowHeights=3*[4*mm])
        tOrd2.setStyle(tstyle1)

         # Modify Measurement table to include PD
        dataOpt[0].append('PD')
        dataOpt[1].append(opData[4])
        dataOpt[2].append(opData[9])
        tOpt2 = Table(dataOpt, colWidths=(50*mm,30*mm,30*mm,25*mm,25*mm,25*mm),
                                rowHeights=3*[4*mm])
        tOpt2.setStyle(tstyle1)

        Story.append(Spacer(1, 6))
        Story.append(tOrd2)
        Story.append(Spacer(1, 12))

        listItems = [("Allfield delivers full-field optics actoss a wide range "
                    "of viewing distances and, like compensation digital or freeform "
                    "lenses, the prescribed powers ordered will not necessarily "
                    "read as expected in a lensometer."),

                    ("Lensometers are designed to measure conventional lenses with "
                    "two surfaces. Allfield lenses are comprosed of two multi-"
                    "element lenses with a total of three fixed surfaces and a "
                    "dynamic, fluid-filled lens."),

                    ("We have interpreted the optics the lensometer will read to "
                    "verify this eyewear was made to the doctor's prescription."),

                    ('<b> PLEASE NOTE: Extreme care must '
                    ' be taken to avoid using the lens clamp when inspecting, as '
                    'this tends to distort the lensometer readings. </b>')]

        for item in listItems:
            my_list = ListFlowable([
                ListItem(Paragraph(item, styleL),
                     leftIndent=55, rightIndent=35,
                     value=u'\u2022',
                     bulletColor=colors.lightblue)],
            bulletType='bullet',
            start='circle',
            leftIndent=25)

            Story.append(my_list)

        Story.append(Spacer(1, 12))
        Story.append(Paragraph("Seen here under optical compensation*:", styleL))
        Story.append(tOpt2)
        Story.append(Spacer(1, 12))

        Story.append(Paragraph("* Adlens verifies the optical power of Allfield "
                            "against the ordered prescription using a proprietary "
                            "imaging system.  This is to determine that the Rx is "
                            "accurate.  This is similar to the optimized "
                            "prescription on freeform progressives.", styleC))

        Story.append(Spacer(1, 6))

        Story.append(Paragraph("** Measurement is performed using a manual "
                            "lensometer. Results will vary depending on lensometer "
                            "measurement type and configuration.  Please allow for "
                            "tolerances on the measured sphere and cylinder powers "
                            "of +/-0.30D and +/-12D for axis.", styleC))

        Story.append(Spacer(1, 12))
        Story.append(tRv1)
        Story.append(tRv2)
        Story.append(Spacer(1, 6))

        # TO ADD 'inquiries...', disposal..., allfield-name.png, CERx.png
        ptext = ['Inquiries',
                'US Inquiries should contact <b> +1 888 459 9793 </b> or <b> askUS@allfieldoptics.com </b>',
                'UK Inquiries should contact <b> 0845 241 1716 </b> or <b> askUK@allfieldoptics.com </b>']
        for item in ptext:
            Story.append(Paragraph(item, styleSL))

        Story.append(Spacer(1, 6))
        ptext = ['Disposal of glasses', 'Please dispose of eyewear in accordance with local regulations']
        for item in ptext:
            Story.append(Paragraph(item, styleSL))

        Story.append(Spacer(1, 18))
        Story.append(afname)
        Story.append(Spacer(1, 24))
        Story.append(CElogo)
        Story.append(Spacer(1, 40))

        ptext = ['Allfield (TM) is a Trademark of ADLENS Ltd. ',
                'This document is Confidential and Proprietary to ADLENS Ltd '
                'and unauthorized use is prohibited']
        for item in ptext:
            Story.append(Paragraph(item, styleSR))

        doc.build(Story)


CreateCertificate.makeForm("987654", ['','','','','','','','','',''], ['','','','','','','','','',''],'','','','','','','','')