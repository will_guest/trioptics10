#-------------------------------------------------------------------------------
# Name:        TriopticsCommands
# Purpose:     Provides an easy-to-use format for delivering commands
#              to the Trioptics via TCP/IP
#
# Author:      Will Guest
#
# Created:     20/01/2015
#-------------------------------------------------------------------------------
import socket, time

class TOP():
    BUFFER_SIZE = 1024

    def tcp_conn(TCP_IP, TCP_PORT):
        global s
        global BUFFER_SIZE
        BUFFER_SIZE = 1024
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TCP_IP, TCP_PORT))

        # Check link to OptiCentric
        s.send("?".encode())
        data0 = s.recv(BUFFER_SIZE).decode()    #receives 'OK' if connected
        if "OK" in data0:
            return "Connection with Trioptics established"
        else:
            return "Connection error, please check settings"
            s.close()
        """Connect to the Trioptics, returns result"""

    def set_ml_radius(rowNumber,radius):
        setmlRadExpr = "set ml radius(" + str(rowNumber) + "," + str(radius) + ")"
        s.send(setmlRadExpr.encode())
        s.recv(BUFFER_SIZE).decode()
        """For a given row (arg1), sets the radius value (arg2)"""

    def tcp_close():
        s.close()
        """Closes TCP connection"""

    def measure():
        s.send("measure".encode())
        #s.recv(BUFFER_SIZE).decode()
        """Functionality of 'Measure' button"""

    def find_center():
        s.send("find_center".encode())
        """Functionality of 'Find Center' button"""

    def set_shutter(numberofms):
        shutterExpr = "set_shutter(" + str(numberofms) + ")"
        s.send(shutterExpr.encode())
        s.recv(BUFFER_SIZE).decode()
        """Provide shutter speed in ms, e.g. '2000'"""

    def get_brightness():
        s.send("get_brightness".encode())
        return s.recv(BUFFER_SIZE).decode()
        """Returns camera brightness in %"""

    def set_objective(headlens):
        setobjExpr = "set_objective(" + str(headlens) + ")"
        s.send(setobjExpr.encode())
        time.sleep(0.2)
        s.recv(BUFFER_SIZE).decode()
        """Provide name of head-lens, e.g. 'PL-200'"""

    def open_df(dfFilename):
        filenameExpr = "open(" + str(dfFilename) + ")"
        s.send(filenameExpr.encode())
        s.recv(BUFFER_SIZE).decode()
        """Provide full path and name of design file to load"""

    def delete_row(rowNumber):
        rowNoExpr = "delete_row(" + str(rowNumber) + ")"
        s.send(rowNoExpr.encode())
        #s.recv(BUFFER_SIZE).decode()
        """Deletes all data for a given row (arg)"""

    def error(rowNumber):
        errorExpr = "error(" + str(rowNumber) + ")"
        s.send(errorExpr.encode())
        return s.recv(BUFFER_SIZE).decode()
        """Returns the centration error of a row (arg)"""

    def live_values_cam():
        s.send("live_values_cam(1)".encode())
        recvd = s.recv(BUFFER_SIZE).decode()
        indEnd = len(recvd)
        ind1 = recvd.find(';',0,indEnd)
        ind2 = recvd.find(';',(ind1+1),indEnd)

        centX = int(float(recvd[:ind1].replace(',','.')))
        centY = int(float(recvd[(ind1+1):ind2].replace(',','.')))
        return (str(centX), str(centY))
        """Returns the camera's live values"""

    def get_results(rowNumber):
        getResExpr = "get results(" + str(rowNumber) + ")"
        s.send(getResExpr.encode())
        return s.recv(BUFFER_SIZE).decode()
        """Returns measurement data of surface (arg)"""

    def get_line(rowNumber):
        getLineExpr = "get_line(" + str(rowNumber) + ")"
        s.send(getLineExpr.encode())
        return s.recv(BUFFER_SIZE).decode()
        """Returns complete line of a single surface (arg)"""

    def get_rel_pos():
        s.send("get rel pos(1)".encode())
        return s.recv(BUFFER_SIZE).decode()
        """Returns the current relative position"""

    def set_zero():
        s.send("set_zero".encode())
        #s.recv(BUFFER_SIZE).decode()
        """Function of 'Set Zero' button"""

    def calc_cent_error():
        s.send("calculate_centering_error".encode())
        #s.recv(BUFFER_SIZE).decode()
        """Calculate centering error"""

    def set_unit(unit):
        unitExpr = "set_unit(" + unit + ")"
        s.send(unitExpr.encode())
        """Set to 'micron' or 'arcmin'"""

    def measure_surface(rowNumber):
        measSurfExpr = "measure surface(" + str(rowNumber) + ")"
        s.send(measSurfExpr.encode())
        """Single surface (arg) measurement"""

    def get_pos(rowNumber):
        getPosExpr = "get_pos(" + str(rowNumber) + ")"
        s.send(getPosExpr.encode())
        return s.recv(BUFFER_SIZE).decode()
        """Returns the relative position of a surface (arg)"""

    def set_manual_rotation(state):
        setManRotExpr = "set manual rotation(" + state + ")"
        s.send(setManRotExpr.encode())
        s.recv(BUFFER_SIZE).decode()
        """Set state of manual rotation ('on'/'off') """

    def set_ml_thickness(rowNumber,thickness):
        setmlThkExpr = "set ml thickness(" + str(rowNumber) + "," + str(thickness) + ")"
        s.send(setmlThkExpr.encode())
        #s.recv(BUFFER_SIZE).decode()
        """Sets the thickness value (arg2) for a given row (arg1)"""

    def set_ml_ri(rowNumber, refrIndex):
        setmlriExpr = "set ml n(" + str(rowNumber) + "," + refrIndex + ")"
        s.send(setmlriExpr.encode())
        #s.recv(BUFFER_SIZE).decode()
        """Sets the refractive index value (arg2) for a given row (arg1)"""

    def set_ml_raw_x(rowNumber, xVal):
        setmlrawxExpr = "set ml raw x(" + str(rowNumber) + "," + xVal + ")"
        s.send(setmlrawxExpr.encode())
        #s.recv(BUFFER_SIZE).decode()
        """Sets the x raw value (arg2) for a given row (arg1)"""

    def set_ml_raw_y(rowNumber, yVal):
        setmlrawyExpr = "set ml raw y(" + str(rowNumber) + "," + yVal + ")"
        s.send(setmlrawyExpr.encode())
        #s.recv(BUFFER_SIZE).decode()
        """Sets the y raw value (arg2) for a given row (arg1)"""

    def set_ml_command(rowNumber, measCom):
        setmlComExpr = "set ml command(" + str(rowNumber) + "," + measCom + ")"
        s.send(setmlComExpr.encode())
        s.recv(BUFFER_SIZE).decode()
        """Sets the measurement command (arg2) for a given row (arg1)"""

    def calc_rel_pos():
        s.send("calc rel pos".encode())
        """Calculate relative positions"""

    def go_to(zPos):
        gotoExpr = "go to(1," + str(zPos) + ")"
        s.send(gotoExpr.encode())
        """Move to a given z-position (arg)"""
