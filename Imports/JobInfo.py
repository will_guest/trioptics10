#-------------------------------------------------------------------------------
# Name:        JobInfo
# Purpose:     Provides connection and data acquisition from Focuss Database
#
# Author:      Will Guest
#
# Created:     10/05/2016
# Copyright:   (c) Will Guest 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import pyodbc

class JobInfo():
    
    def split_values(arg):
    # Handle missing data in table (return 'None' where req'd)
        if ";" in str(arg):
            return arg.split(';', 1)
        else:
            return (None, None)

    def get_data(jobID):
        # Connect to Annapurna and get 'jobdata'
        cnxn = pyodbc.connect('DSN=Annapurna;PWD=Will')
        cursor = cnxn.cursor()
        try:
            SQLrx = cursor.execute("SELECT jobdata "
                                    "FROM jobs WHERE autojobnum = " + jobID)
            jobData = list(SQLrx.fetchone())

        except ValueError:  return ValueError
        except IOError:     return IOError
        except TypeError:   return TypeError

        # Convert data from table into dictionary
        myArr = jobData[0].split('\r\n')
        myDict = dict(elem.strip().split('=', 1) for elem in myArr[:-1])

        # Get values from dictionary
        accNo = myDict.get('USER')
        frameStl = myDict.get('LibFramedescrip').split()[1]
        frameCol = myDict.get('Colourdescrip')

        (rPD, lPD)   = JobInfo.split_values(myDict.get('pd'))
        (rSph, lSph) = JobInfo.split_values(myDict.get('Sph'))
        (rCyl, lCyl) = JobInfo.split_values(myDict.get('Cyl'))
        (rAxs, lAxs) = JobInfo.split_values(myDict.get('Axis'))
        (rAdd, lAdd) = JobInfo.split_values(myDict.get('Add'))

        return (jobID, accNo, frameStl, frameCol, 
                rPD, lPD, rSph, lSph, rCyl, lCyl, rAxs, lAxs, rAdd, lAdd)

    def get_orderdata(jobID):
        # Connect to Annapurna and get 'jobdata'
        cnxn = pyodbc.connect('DSN=Annapurna;PWD=Will')
        cursor = cnxn.cursor()

        try:
            SQLget = cursor.execute("SELECT AccountNo, Reference, EnteredDate "
                                    "FROM orders WHERE autojobnum = " + jobID)
            res2 = list(SQLget.fetchone())
        except ValueError:  return ValueError
        except IOError:     return IOError
        cursor.close()
        return res2

    def init_focuss_conn():
        return pyodbc.connect('Driver={SQL Server Native Client 11.0};'
                       'Server=tcp:zcxd3jvhs4.database.windows.net,1433;'
                       'Database=Focuss;'
                       'Uid=nasuni_admin;Pwd=Specs4@ll;Encrypt=yes;'
                       'TrustServerCertificate=no;Connection Timeout=30;')

    def upsert_record_in_JobInfo(jobID, jobData):
        connection = JobInfo.init_focuss_conn()
        cursor = connection.cursor()
        SQLchk = cursor.execute("SELECT TOP 1 JobNo "
                                "FROM JobInfo WHERE JobNo = " + jobID)
        recChk = SQLchk.fetchone()
        print(recChk)

        if recChk is None:
            print (str(jobID) + " will be added to JobInfo")
            SQLadd = ("Insert INTO JobInfo "
                        "(JobNo, AccountNo, Frame_Style, Frame_Colour, "
                        "PD_Right, PD_Left, Sphere_Right, Sphere_Left, "
                        "Cylinder_Right, Cylinder_Left, Axis_Right, Axis_Left, "
                        "Add_Right, Add_Left) "
                        "Values (" + ((len(jobData)-1)*'?,' + '?') + ")")
            cursor.execute(SQLadd, jobData)
            connection.commit()

        else:
            SQLupd = ("UPDATE JobInfo "
                    "SET JobNo = ?, AccountNo = ?, "
                    "Frame_Style = ?, Frame_Colour = ?, "
                    "PD_Right = ?, PD_Left = ?, "
                    "Sphere_Right = ?, Sphere_Left = ?, "
                    "Cylinder_Right = ?, Cylinder_Left = ?, "
                    "Axis_Right = ?, Axis_Left = ?, "
                    "Add_Right = ?, Add_Left = ? "
                    "WHERE JobNo = " + jobID)
            print (SQLupd)
            cursor.execute(SQLupd, jobData)

        connection.close()