#-------------------------------------------------------------------------------
# Name:         Module 10 Report
# Purpose:      Calculates optical powers, displays and prints results
# Author:       Will Guest
# Created:      17/04/2015
# Copyright:    (c) Will Guest 2015
#-------------------------------------------------------------------------------

import sys, time, math, csv
import pyodbc
from os import path, getcwd
from operator import sub
from datetime import datetime
from PyQt5 import uic, Qt, QtCore, QtGui

guiFile = path.join(path.dirname(sys.argv[0]), 'Report10.ui')
form_report = uic.loadUiType(guiFile, True)[0]

class RepWindow(QtGui.QMainWindow, form_report):

    def __init__(self, parent=None):
        # Initialise main window
        super(QtGui.QMainWindow,self).__init__(parent=None)
        QtGui.QMainWindow.setWindowFlags(self, QtCore.Qt.WindowStaysOnTopHint)
        self.setupUi(self)
        self.btnPrint.clicked.connect(self.print_to_file)

    #-------------------- LENS POWER CALCULATIONS --------------------------#

    def calc_Powers(self, refL, verFoc, horFoc):
        float(refL)
        if refL != 0:
            if refL == 2:
                refLfoc = -500.0
            elif refL == 5:
                refLfoc = -200.0
            diop1 = ((1000/-float(verFoc))-(1000/-refLfoc))/(1-(1.1/-refLfoc))
            diop2 = ((1000/-float(horFoc))-(1000/-refLfoc))/(1-(1.1/-refLfoc))
        else:
            diop1 = (1000/-float(verFoc))
            diop2 = (1000/-float(horFoc))

        sph = round(max(diop1, diop2), 2)
        cyl = round(min(diop1,diop2)-max(diop1, diop2),2)
        return(sph, cyl)

    def calc_Prism(self, hand, xCent, yCent, avPower):
        if hand == 'Rt':
            hPr = round((-float(xCent)/(5*avPower)), 2)
        elif hand == 'Lt':
            hPr = round((float(xCent)/(5*avPower)), 2)

        if hPr < 0:     hPrDir = "In"
        else:           hPrDir = "Out"

        vPr = round((-float(yCent)/(5*avPower)), 2)
        if vPr > 0:     vPrDir = "Up"
        else:           vPrDir = "Down"
        return (hPrDir, hPr, vPrDir, vPr)

    #-----------------------POPULATE FORM ----------------------------------#

    def batchInfo(self, jobID, frameStyle, frameColour,
                        instrID_TOP, instrID_FOC, fixtureID):
        self.lblBatchID.setText(str(jobID))
        self.lblBatchID_barcode.setText("*"+ jobID +"*")
        self.lblFrameStyle.setText(str(frameStyle))

        self.lblFrameColour.setText(str(frameColour))

        fontsize = (19 - 0.5*len(frameColour))
        font = QtGui.QFont("Calibri", fontsize)
        self.lblFrameColour.setFont(font)

        self.instrID_TOP.setText(instrID_TOP)
        self.fixtureID_TOP.setText(str(fixtureID))
        self.instrID_FOC.setText(instrID_FOC)

    def rxInfo(self, hand, data):
        eval("self." + hand + "PD").setText(data[0])
        eval("self." + hand + "RxSph").setText(data[1])
        eval("self." + hand + "RxCyl").setText(data[2])
        eval("self." + hand + "RxAxs").setText(data[3])
        eval("self." + hand + "RxAdd").setText(data[4])

    def modNos(self, rModID, lModID):
        self.Rt_Mod_ID.setText(rModID)
        self.Rt_Mod_ID_barcode.setText("*"+rModID+"*")
        self.Lt_Mod_ID.setText(lModID)
        self.Lt_Mod_ID_barcode.setText("*"+lModID+"*")

    def uMeasuredData(self, hand, refL, data):
        # Display raw data on report
        eval("self.raw" + hand + "UVer.setText('" + str(data[0]) + "')")
        eval("self.raw" + hand + "UHor.setText('" + str(data[1]) + "')")
        eval("self.raw" + hand + "Uxct.setText('" + str(int(data[3])) + "')")
        eval("self.raw" + hand + "Uyct.setText('" + str(int(data[4])) + "')")

        # Calculate dioptric powers and display on report
        sph, cyl = self.calc_Powers(refL, data[0], data[1])
        eval("self." + hand + "DSph.setText('" + "{0:.2f}".format(sph) + "')")
        eval("self." + hand + "DCyl.setText('" + "{0:.2f}".format(cyl) + "')")
        eval("self." + hand + "DAxs.setText('" + str(int(data[2])) + "')")

        # Calculate prism and display on report
        avPower = (float(data[0])+float(data[1]))/2
        hPrDir, hPr, vPrDir, vPr = self.calc_Prism(hand, data[3], data[4], avPower)
        eval("self." + hand + "DHPRdir.setText('" + hPrDir + "')")
        eval("self." + hand + "DHPRmag.setText('" + "{0:.2f}".format(abs(hPr)) + "')")
        eval("self." + hand + "DVPRdir.setText('" + vPrDir + "')")
        eval("self." + hand + "DVPRmag.setText('" + "{0:.2f}".format(abs(vPr)) + "')")

    def aMeasuredData(self, hand, refL, data):
        # Display raw data on report
        eval("self.raw" + hand + "AVer.setText('" + str(data[0]) + "')")
        eval("self.raw" + hand + "AHor.setText('" + str(data[1]) + "')")
        eval("self.raw" + hand + "Axct.setText('" + str(int(data[3])) + "')")
        eval("self.raw" + hand + "Ayct.setText('" + str(int(data[4])) + "')")

        # Calculate dioptric powers and display on report
        sph, cyl = self.calc_Powers(refL, data[0], data[1])
        eval("self." + hand + "NSph.setText('" + "{0:.2f}".format(sph) + "')")
        eval("self." + hand + "NCyl.setText('" + "{0:.2f}".format(cyl) + "')")
        eval("self." + hand + "NAxs.setText('" + str(int(data[2])) + "')")

    def calcMeridErr(self, hand, tolerance):
        th = list(range(1,180))
        mSph = float(eval("self." + hand + "DSph.text()"))
        mCyl = float(eval("self." + hand + "DCyl.text()"))
        mAxs = math.radians(float(eval("self." + hand + "DAxs.text()")))

        tSph = float(eval("self." + hand + "RxSph.text()"))
        tCyl= float(eval("self." + hand + "RxCyl.text()"))
        tAxs = math.radians(float(eval("self." + hand + "RxAxs.text()")))

        pow_m = list(mSph + mCyl/2 - mCyl/2 * math.cos(2*(math.radians(i)-mAxs)) for i in th)
        pow_t = list(tSph + tCyl/2 - tCyl/2 * math.cos(2*(math.radians(i)-tAxs)) for i in th)
        pow_diff = map(sub, pow_m, pow_t)
        meridErr = round(max(abs(val) for val in pow_diff), 1)

        eval("self." + hand + "MeridErr.setText('" + "{0:.1f}".format(meridErr) + "')")

        if meridErr <= tolerance:   res = "Pass"
        else:                       res = "Fail"
        eval("self." + hand + "MeridErrRes.setText('" + res + "')")

    def calcAddErr(self, hand, tolerance):
        NSph = float(eval("self." + hand + "NSph.text()"))
        NNES = (float(eval("self." + hand + "NSph.text()"))
                + float(eval("self." + hand + "NCyl.text()"))/2)

        DNES = (float(eval("self." + hand + "DSph.text()"))
                + float(eval("self." + hand + "DCyl.text()"))/2)

        NESAdd = round(round(NNES,3) - round(DNES,3), 2)
        eval("self." + hand + "NESAdd.setText('" + str(NESAdd) + "')")

        RxAdd = float(eval("self." + hand + "RxAdd.text()"))
        AddErr = NESAdd - RxAdd
        eval("self." + hand + "AddErr.setText('" + "{0:.2f}".format(AddErr) + "')")

        if -tolerance <= AddErr <= tolerance:   res = "Pass"
        else:                                   res = "Fail"
        eval("self." + hand + "AddErrRes.setText('"  + res + "')")
        return AddErr

    def calcNESImb(self, RtAddErr, LtAddErr, tolerance):
        NESImb = round(abs(RtAddErr-LtAddErr),2)
        self.NESimb.setText(str(NESImb))
        if NESImb <= tolerance:                 res = 'Pass'
        else:                                   res = 'Fail'
        self.NESimbRes.setText(res)

    def FOCmeasuredData(self, hand, data):
        # Display measurement data on report
        eval("self.Foc" + hand + "USph.setText('" + "{0:.2f}".format(float(data[0])) + "')")
        eval("self.Foc" + hand + "UCyl.setText('" + "{0:.2f}".format(float(data[1])) + "')")
        eval("self.Foc" + hand + "UAxs.setText('" + str(int(data[2])) + "')")
        eval("self.Foc" + hand + "UPsm.setText('" + "{0:.2f}".format(float(data[3])) + "')")
        eval("self.Foc" + hand + "UAng.setText('" + str(int(data[4])) + "')")

        eval("self.Foc" + hand + "ASph.setText('" + "{0:.2f}".format(float(data[5])) + "')")
        eval("self.Foc" + hand + "ACyl.setText('" + "{0:.2f}".format(float(data[6])) + "')")
        eval("self.Foc" + hand + "AAxs.setText('" + str(int(data[7])) + "')")

        # Calculate and display cartesian prism data on report
        hPsm = (float(data[3])*math.sin(math.radians(int(data[4])+90)))
        vPsm = (float(data[3])*math.cos(math.radians(int(data[4])-90)))

        if (hPsm > 0 and hand == "Rt") or (hPsm < 0 and hand == "Lt"):
            hPsmDir = "In"
        else:
            hPsmDir = "Out"

        if vPsm < 0:        vPsmDir = "Dn"
        else:               vPsmDir = "Up"

        eval("self.Foc" + hand + "HorPsmDir.setText('" + hPsmDir + "')")
        eval("self.Foc" + hand + "HorPsm.setText('" + "{0:.2f}".format(abs(hPsm)) + "')")
        eval("self.Foc" + hand + "VerPsmDir.setText('" + vPsmDir + "')")
        eval("self.Foc" + hand + "VerPsm.setText('" + "{0:.2f}".format(abs(vPsm)) + "')")

        # Assess against tolerances and give results
        if (-2.33 <= float(hPsm) <= 3.00 and abs(float(vPsm)) <= 0.33):
                PrRes = "Pass"
        else:   PrRes = "Fail"

        eval("self.Foc" + hand + "PsmRes.setText('" + str(PrRes) + "')")
        return (float(hPsm), float(vPsm))

    def calcPrismImb(self, hPrRt, hPrLt, vPrRt, vPrLt, hTolL, hTolU, vTol):
        hImb = "{0:.2f}".format(hPrLt - hPrRt)
        vImb = "{0:.2f}".format(abs(vPrRt - vPrLt))
        self.FocHorPsmImb.setText(hImb)
        self.FocVerPsmImb.setText(vImb)

        if hTolL <= float(hImb) <= hTolU:   hImbRes = "Pass"
        else:                               hImbRes = "Fail"
        self.FocHorPsmImbRes.setText(hImbRes)

        if abs(float(vImb)) <= vTol:             vImbRes = "Pass"
        else:                               vImbRes = "Fail"
        self.FocVerPsmImbRes.setText(vImbRes)

    def comments(self, txtComments):
        if str(txtComments) == "":
            self.lblComments.setText("None")
        else:
            self.lblComments.setText(str(txtComments))

    def finalResult(self):
        if (self.RtMeridErrRes.text()   == "Pass" and
            self.RtAddErrRes.text()     == "Pass" and
            self.LtMeridErrRes.text()   == "Pass" and
            self.LtAddErrRes.text()     == "Pass" and
            self.NESimbRes.text()       == "Pass" and
            self.FocRtPsmRes.text()     == "Pass" and
            self.FocLtPsmRes.text()     == "Pass" and
            self.FocHorPsmImbRes.text() == "Pass" and
            self.FocVerPsmImbRes.text() == "Pass"):
            self.FinalResult.setText("PASS")
        else:
            self.FinalResult.setText("FAIL")


    #-------------------- SAVING DATA & PRINTING --------------------------#

    def add_to_10Optics(self, newData):
        connection = pyodbc.connect('Driver={SQL Server Native Client 11.0};'
                       'Server=tcp:zcxd3jvhs4.database.windows.net,1433;'
                       'Database=Focuss;'
                       'Uid=nasuni_admin;Pwd=Specs4@ll;Encrypt=yes;'
                       'TrustServerCertificate=no;Connection Timeout=30;')
        cursor = connection.cursor()
        SQLadd = ("Insert INTO [dbo].[10Optics] "
                    "(JobNo, PartNo, Date, Time, Operator, Hand, "
                    "uRefLens, uVertical, uHorizontal, "
                    "uXcentration, uYcentration, "
                    "uSphere, uCylinder, uAxis, "
                    "uHorPrism, uHorPrismDir, uVerPrism, uVerPrismDir, "
                    "aRefLens, aVertical, aHorizontal, "
                    "aXcentration, aYcentration, "
                    "aSphere, aCylinder, aAxis, "
                    "MeridError, AddError, "
                    "FocHorPrismMag, FocHorPrismDir, FocVerPrismMag, FocVerPrismDir, "
                    "FocHorPrismImb, FocVerPrismImb, Comments)"
                    "Values (" + ((len(newData)-1)* '?,' + '?') + ")")
        cursor.execute(SQLadd, newData)
        connection.commit()
        connection.close()

    def print_to_excel(self, hand):
        now = datetime.today()
        dateStamp = now.strftime('%d %b %Y')
        timeStamp = now.strftime('%H:%M')
        operator = "QC"
        newData = (self.lblBatchID.text(),
                    eval('self.'+ hand +'_Mod_ID.text()'),
                    dateStamp, timeStamp, operator, hand,
                    eval('self.'+ hand +'uRef.text()'),
                    eval('self.raw'+ hand +'UVer.text()'),
                    eval('self.raw'+ hand +'UHor.text()'),
                    eval('self.raw'+ hand +'Uxct.text()'),
                    eval('self.raw'+ hand +'Uyct.text()'),
                    eval('self.'+ hand +'DSph.text()'),
                    eval('self.'+ hand +'DCyl.text()'),
                    eval('self.'+ hand +'DAxs.text()'),
                    eval('self.'+ hand +'DHPRmag.text()'),
                    eval('self.'+ hand +'DHPRdir.text()'),
                    eval('self.'+ hand +'DVPRmag.text()'),
                    eval('self.'+ hand +'DVPRdir.text()'),
                    eval('self.'+ hand +'aRef.text()'),
                    eval('self.raw'+ hand +'AVer.text()'),
                    eval('self.raw'+ hand +'AHor.text()'),
                    eval('self.raw'+ hand +'Axct.text()'),
                    eval('self.raw'+ hand +'Ayct.text()'),
                    eval('self.'+ hand +'NSph.text()'),
                    eval('self.'+ hand +'NCyl.text()'),
                    eval('self.'+ hand +'NAxs.text()'),
                    eval('self.'+ hand +'MeridErr.text()'),
                    eval('self.'+ hand +'AddErr.text()'),
                    eval('self.Foc'+ hand +'HorPsm.text()'),
                    eval('self.Foc'+ hand +'HorPsmDir.text()'),
                    eval('self.Foc'+ hand +'VerPsm.text()'),
                    eval('self.Foc'+ hand +'VerPsmDir.text()'),
                    self.FocHorPsmImb.text(),
                    self.FocVerPsmImb.text(),
                    self.lblComments.text())

        directory = "T:/MEASUREMENTS/Trioptics/10.OpticalRelease/"
        fileName = "QS-F-070 Optical Performance Datasheet.csv"
        with open(directory + fileName, 'a', newline='') as fileOutput:
            writer = csv.writer(fileOutput, delimiter=',')
            writer.writerow(newData)
        fileOutput.close

        self.add_to_10Optics(newData)

    def print_to_file(self):
        # Print To File
        printer = QtGui.QPrinter()
        directory = "T:/MEASUREMENTS/Trioptics/10.OpticalRelease/pdf/"
        filename = self.lblBatchID.text() + ".pdf"

        printer.setOutputFormat(QtGui.QPrinter.PdfFormat)
        printer.setOutputFileName(directory + filename)
        printer.setPageMargins(5,8,5,5,0)
        printer.setResolution(108)

        painter = QtGui.QPainter()
        QtGui.QMainWindow.move(self, 500, 50)

        painter.begin(printer)
        self.centralwidget.render(painter)
        painter.end()
        del painter, printer

        self.print_to_excel("Rt")
        self.print_to_excel("Lt")

        self.print_report()

    def print_report(self):
        # Print To Printer
        printer = QtGui.QPrinter()
        printer.setPageMargins(5,8,5,5,0)
        printer.setResolution(108)
        painter = QtGui.QPainter()

        painter.begin(printer)
        dialog = QtGui.QPrintDialog()
        if dialog.exec_() == QtGui.QDialog.Accepted:
            self.centralwidget.render(painter)
        painter.end()
        del painter, printer


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    myWindow = RepWindow()
    myWindow.show()
    sys.exit(app.exec_())